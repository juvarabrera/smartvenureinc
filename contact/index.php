<html>
<head>
<title>Smart Venure Inc. - Contact</title>
	<link href='http://fonts.googleapis.com/css?family=Numans' rel='stylesheet' type='text/css'>
	<link href='../styles/style.css' rel='stylesheet' type='text/css'>
</head>
<body>
<center>
<?php
require_once('../require/svTop.php');
?>
<div id="svContent">
<h1>Contact</h1><div id="divider"></div>
<span>
<table width="100%">
	<tr valign="top">
		<td width="50%">
		<table cellpadding="10">
			<tr>
				<td><b>Full Name:</b></td>
				<td><input type="text" name="name" placeholder="Full Name"></td>
			</tr>
			<tr>
				<td><b>Email:</b></td>
				<td><input type="text" name="email" placeholder="Email"></td>
			</tr>
			<tr valign="top">
				<td><b>Message:</b></td>
				<td><textarea name="message" placeholder="Message"></textarea></td>
			</tr>
			<tr valign="top">
				<td></td>
				<td><input type="submit" value="Send"></td>
			</tr>
		</table>
		</td>
		<td>
		<table cellpadding="10">
			<tr>
				<td><b>Contact person:</b></td>
				<td>Klaudin Emili KIamzon</td>
			</tr>
			<tr>
				<td><b>Email address:</b></td>
				<td>info@smartventureinc.com</td>
			</tr>
			<tr valign="top">
				<td><b>Contact numbers:</b></td>
				<td>09175278351<br>(046)414-8325</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="http://www.facebook.com/smartventure" style="margin-right:10px;" target="_blank"><img src="../images/skin/default/bg/facebook.png"></a>
					<a href="http://www.twitter.com/smartventureinc" style="margin-right:10px;" target="_blank"><img src="../images/skin/default/bg/twitter.png"></a>
					<a href="http://www.instagram.com/smartventureinc" style="margin-right:10px;" target="_blank"><img src="../images/skin/default/bg/instagram.png"></a>
					<br><br>
					<img src="../images/skin/default/bg/bbcode.jpg">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</span>
<br><br><br><br>
<h1>Location</h1><div id="divider"></div>
<span><center><img src="../images/skin/default/bg/loc.jpg"><br><br>Dasmariņas, Technopark, Blk.13 Lot 9 Area G.1 DBB,<br>Paliparan I, Dasmariņas City Cavite, Philippines 4114.</center></span>
</div>
<?php
require_once('../require/svFooter.php');
?>
<div id="svCopyright">
SmartVenue Inc. &copy; 2013<br>
Web design by Juvar Abrera.
</div>
</body>
</html>