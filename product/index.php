<html>
<head>
<title>Smart Venure Inc. - Product</title>
	<link href='http://fonts.googleapis.com/css?family=Numans' rel='stylesheet' type='text/css'>
	<link href='../styles/style.css' rel='stylesheet' type='text/css'>
</head>
<body>
<center>
<?php
require_once('../require/svTop.php');
?>
<div id="svContent">
<h1>Zeus Viewer</h1><div id="divider"></div>
<img src="../images/skin/default/bg/product1.jpg" width="420px" style="margin:20px;"><img src="../images/skin/default/bg/product2.jpg" width="420px" style="margin:20px;"><span>Zeus Viewer has a distinct characteristic of converting any ordinary LED or LCD Television into a SMART Television which grant users a surpassing entertainment experience than the ordinary one. This product marks an excellent choice of using Television for households, schools and office because of its convenience and efficiency for conference, presentation and lecturing. Zeus� performance and reliability offers customer satisfaction due to Multi-media Entertainment at home.</span>
<h1>Features</h1><div id="divider"></div>
<img src="../images/skin/default/bg/product3.jpg" width="420px" style="margin:20px;"><img src="../images/skin/default/bg/product4.jpg" width="420px" style="margin:20px;">
<span>
<ul>
	<li>Android 4.0.4</li>
	<li>1GB DDR3 RAM</li>
	<li>8GB ROM</li>
	<li>Micro SD card support up to 32GB</li>
	<li>5.0 M AF Camera</li>
	<li>Mic and advanced audio enhancement</li>
	<li>HDMI: 1080 output</li>
	<li>Wifi standard</li>
	<li>Interface: DC, Mini USB, USB Host, micro SD/TF, HDMI, AV Jack</li>
	<li>With touchpad and airmouse</li>
</ul>
</span>
</div>
<?php
require_once('../require/svFooter.php');
?>
<div id="svCopyright">
SmartVenue Inc. &copy; 2013<br>
Web design by Juvar Abrera.
</div>
</body>
</html>