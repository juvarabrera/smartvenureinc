<html>
<head>
<title>Smart Venure Inc. - About</title>
	<link href='../styles/style.css' rel='stylesheet' type='text/css'>
</head>
<body>
<center>
<?php
require_once('../require/svTop.php');
?>
<div id="svContent">
<h1>Vision</h1><div id="divider"></div>
<img src="../images/skin/default/bg/vision.png" style="float:right"><span>SmarTVenture is a company of promise and possibilities managed by innovative minds that aims to develop and heighten the viewing experience of their customers locally. Guided by its company core values, SmarTVenture sees itself as a leader in electronics industry participating meaningfully in manufacturing world class television converter by consistently providing reliable, high quality, and affordable products with unparalleled service.</span>
<br><br><br><br>
<h1>Mission</h1><div id="divider"></div>
<img src="../images/skin/default/bg/mission.jpg" style="float:left"><span>We strive to be the primary and top leading manufacturer of Smart TV converter that uses an integrated technology of Wifi, Bluetooth, and Android, thru benchmarking and improving the latest technology available and providing high quality products and services to customers to fully satisfy their needs and wants. The company shall continue to transform ideas into possibilities, providing great experience of technological innovations considering the value of both man and the environment.</span>
<br><br><br><br>
<h1>Core Values</h1><div id="divider"></div>
<img src="../images/skin/default/bg/corevalues.jpg" style="float:right"><span>
<ul>
	<li>Commitment  Committing to great product, service, and other initiatives that impact lives within and outside the organization</li>
	<li>Innovation  Pursuing new creative ideas that have the potential to change the world.</li>
	<li>Ownership  Taking care of the company and customers as they were ones own.</li>
	<li>Empowerment  Encouraging employees to take initiative and give the best. Adopting an error-embracing environment to empower employees to lead and make decisions.</li>
	<li>Accountability  Acknowledging and assuming responsibility for actions, products, decisions, and policies. It can be applied to both individual accountability on the part of employees and accountability of the company as a whole.</li>
	<li>Diversity  respecting the diversity and giving the best of composition. Establishing an employee equity program.</li>
</ul>
</span>
</div>
<?php
require_once('../require/svFooter.php');
?>
<div id="svCopyright">
SmartVenue Inc. &copy; 2013<br>
Web design by Juvar Abrera.
</div>
</body>
</html>