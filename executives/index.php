<html>
<head>
<title>Smart Venure Inc. - About</title>
	<link href='../styles/style.css' rel='stylesheet' type='text/css'>
</head>
<body>
<center>
<?php
require_once('../require/svTop.php');
?>
<div id="svContent">
<h1>Executive Profiles</h1><div id="divider"></div>
<table width="100%" cellspacing="0" cellpadding="10">
	<tr>
		<td width="1px"><img src="../images/skin/default/executives/1.jpg"></td>
		<td><b>Klaudin Emili A. Kiamzon</b><br><i>President</i></td>
		<td width="1px"><img src="../images/skin/default/executives/2.jpg"></td>
		<td><b>Artneil Christopher Tomagos</b><br><i>Vice President</i></td>
	</tr>
	<tr>
		<td width="1px"><img src="../images/skin/default/executives/3.jpg"></td>
		<td><b>Lexter Anabe</b><br><i>General Manager</i></td>
		<td width="1px"><img src="../images/skin/default/executives/4.jpg"></td>
		<td><b>Maria Suzaine P. Manguerra</b><br><i>Sales Manager</i></td>
	</tr>
	<tr>
		<td width="1px"><img src="../images/skin/default/executives/5.jpg"></td>
		<td><b>Joy Ann Marie Zafra</b><br><i>Manufacturing Director</i></td>
		<td width="1px"><img src="../images/skin/default/executives/6.jpg"></td>
		<td><b>Zyra Jane Atendido</b><br><i>Research and Development Engineer</i></td>
	</tr>
	<tr>
		<td width="1px"><img src="../images/skin/default/executives/7.jpg"></td>
		<td><b>Joseph Obusan</b><br><i>Quality Assurance Engineer</i></td>
		<td width="1px"></td>
		<td></td>
	</tr>
</table>
</div>
<?php
require_once('../require/svFooter.php');
?>
<div id="svCopyright">
SmartVenue Inc. &copy; 2013<br>
Web design by Juvar Abrera.
</div>
</body>
</html>